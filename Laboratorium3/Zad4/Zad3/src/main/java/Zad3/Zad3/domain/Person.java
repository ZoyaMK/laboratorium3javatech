package Zad3.Zad3.domain;

public class Person {

    private String name;
    private String lastName;
    private String email;
    private String companyName;
  
    public Person() {
      System.out.println("Creating person " + this);
    }
  
    public Person(String name, String lastName, String email, String companyName) {
      this.name = name;
      this.lastName = lastName;
      this.email = email;
      this.companyName = companyName;
      System.out.println("Creating person " + this);
    }

    public Ojciec extends Person{

    }

    public Matka extends Person{

    }

    public Dziecko extends Person{

    }

    public Rodzina extends Ojciec, Matka, Dziecko{
      
    }
  
    public String getName() {
      return name;
    }
  
    public void setName(String name) {
      this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setlastName(String lastName){
        this.lastName = lastName;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getcomapnyName(){
        return companyName;
    }

    public void setcompanyName(String companyName){
        this.companyName = companyName;
    }


    
  
    @Override
    public String toString() {
      return "Person{" +
          "name ='" + name + '\'' +
          ", lastName =" + lastName +
          ", email =" + email +
          ", Company Name =" + companyName +
          '}';
    }
  }
package Zad3.Zad3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import Zad3.Zad3.domain.Person;

@Service
public class Zad3Service{
    @Autowired
    @Qualifier("prezes")
    Person prezydent;

    Person getPrezes(){
        return prezydent;
    }

    @Qualifier("wiceprezes")
    Person wiceprezydent;

    Person getWicePrezes(){
        return wiceprezydent;
    }

    @Qualifier("sekretarka")
    Person sekretarka;

    Person getSekretarka(){
        return sekretarka;
    }
}
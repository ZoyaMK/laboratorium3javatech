package Zad3.Zad3.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import Zad3.Zad3.domain.Person;

@Configuration
public class Zad3ServiceConfig{

    @Bean
    public Person prezes(){
        return new Person("Chrystal", "Havoc", "chavocr@yahoo.com", "Mymm");
    }

    public Person wiceprezes(){
        return new Person("Halley", "Gdaud", "hgdaud9@sohu.com", "Oyope");
        
    }

    public Person sekretarka(){
        return new Person("Kirbie", "Wrettum", "kwrettumj@slideshare.net", "Browsetyp");
    }

}
package Zad3.Zad3;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import Zad3.Zad3.domain.Person;

@SpringBootApplication
public class Zad3Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Zad3Application.class, args);

		Person person = (Person) context.getBean("prezes");
		System.out.println(person.getName());

		Person person2 = (Person) context.getBean("wiceprezes");
		System.out.println(person2.getName());

		Person person3 = (Person) context.getBean("sekretarka");
		System.out.println(person3.getName());

		
	}

}
